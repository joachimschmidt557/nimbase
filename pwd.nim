import os, cligen
import common

proc main() =
    echo getCurrentDir()

dispatch(main, version=("version", nimbaseVersion))
