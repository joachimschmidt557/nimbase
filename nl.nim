import cligen
import common

proc digits(num:int): int =
    result = 1
    var x = num
    while x div 10 >= 1:
        inc result
        x = x div 10

proc spaces(num:int): string =
    for i in 1..num:
        result &= " "

proc formatNumber(num:int, width:int): string =
    result = if digits(num) > width: $num
             else: spaces(width - digits(num)) & $num

proc main(files:seq[string], lineIncrement=1, numberSeparator="\t", numberWidth=6) =
    var lineNr = 0
    for file in files:
        for line in lines(file):
            lineNr += lineIncrement
            writeLine(stdout, formatNumber(lineNr, numberWidth) & numberSeparator & line)

dispatch(main, version=("version", nimbaseVersion))
