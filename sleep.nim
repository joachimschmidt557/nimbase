import os, cligen, sequtils
import common

proc timeToSecs(time:string): int =
    result = 0

proc main(secs:seq[int]) =
    sleep(secs.foldl(a + b) * 1000)

dispatch(main, version=("version", nimbaseVersion))
