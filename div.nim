import cligen, sequtils
import common

proc main(integers:seq[int]) =
    if integers.len == 2:
        echo integers[0] / integers[1]

dispatch(main, version=("version", nimbaseVersion))
