import cligen, os, unicode
import common

proc catFile(file:string) =
    if not existsFile(file):
        err "rev: " & file & ": No such file or directory"
        quit 1
    var
        f = open(file, fmRead)
    for line in lines(f):
        writeLine(stdout, reversed(line))

proc catStdin() =
    for line in lines(stdin):
        writeLine(stdout, reversed(line))

proc main(files:seq[string]) =
    if files.len == 0:
        catStdin()
    for file in files:
        if file == "-":
            catStdin()
        else:
            catFile(file)

dispatch(main, version=("version", nimbaseVersion))
