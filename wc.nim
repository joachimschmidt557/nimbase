import cligen, os, strutils
import common

proc main(files:seq[string]) =
#    if files.len == 0:
#        files.add "-"
    var
        totalLines = 0
        totalWords = 0
        totalChars = 0
    for file in files:
        var f = if file == "-": stdin
                else: open(file, fmRead)
        var
            fileLines = 0
            fileWords = 0
            fileChars = 0
        for line in lines(f):
            inc fileLines
            fileWords += line.split.len
            fileChars += line.len
        totalLines += fileLines
        totalWords += fileWords
        totalChars += fileChars
        writeLine(stdout, $(fileLines) & $(fileWords) & $(fileChars) & file)
    if files.len > 1:
        writeLine(stdout, $(totalLines) & $(totalWords) & $(totalChars) & "total")

dispatch(main, version=("version", nimbaseVersion))
