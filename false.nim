import cligen
import common

proc main() = 
    quit(1)

dispatch(main, version=("version", nimbaseVersion))
