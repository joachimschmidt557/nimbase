import cligen, os, math, deques
import common

proc main(files:seq[string], lines=10, bytes=0, quiet=false) =
#    if files.len == 0:
#        files.add "-"
    for file in files:
        if files.len > 1 and not quiet:
            writeLine(stdout, "==> " & (if file == "-": "standard input" else: file) & " <==")
        var f = if file == "-": stdin
                else: open(file, fmRead)
        if bytes > 0:
            var q = initDeque[char]()
            while not f.endOfFile:
                if q.len > bytes:
                    q.popFirst
                q.addLast(f.readChar)
            for item in q.items:
                write(stdout, item)
        elif lines > 0:
            var q = initDeque[string]()
            for line in lines(f):
                if q.len > lines:
                    q.popFirst
                q.addLast line
            for line in q.items:
                writeLine(stdout, line)

dispatch(main, version=("version", nimbaseVersion))
