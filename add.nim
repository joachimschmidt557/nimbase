import cligen, sequtils
import common

proc main(integers:seq[int]) =
    if integers.len > 0:
        echo integers.foldl(a + b)

dispatch(main, version=("version", nimbaseVersion))
