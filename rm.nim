import cligen, os
import common

const
    errorMsg = "rm: cannot remove '"

proc main(files:seq[string], force=false, recursive=false) =
    for file in files:
        if existsFile(file):
            removeFile(file)
        elif existsDir(file):
            if recursive:
                removeDir(file)
            else:
                err errorMsg & file & "': Is a directory"
                programResult = 1
        else:
            err errorMsg & file & "': No such file or directory"
            programResult = 1

dispatch(main, version=("version", nimbaseVersion))
