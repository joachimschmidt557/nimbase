import cligen, terminal
import common

proc redraw(lineBuffer:seq[string], topLine:int) =
    eraseScreen(stdout)
    for i in topLine .. topLine + terminalHeight():
        writeLine(stdout, lineBuffer[i])
    let percent = (100 * (topLine / lineBuffer.len)).int
    stdout.styledWrite(fgBlack, bgWhite, "--More--(" & $(percent) & "%)")

proc main(files:seq[string]) =
    if files.len == 0:
        err "more: bad usage"
        quit 1
    addQuitProc(resetAttributes)
    var lineBuffer:seq[string]
    for file in files:
        for line in lines(file):
            lineBuffer.add(line)
    if lineBuffer.len < terminalHeight():
        for line in lineBuffer:
            writeLine(stdout, line)
        return
    let
        minTopLine = 0
        maxTopLine = lineBuffer.high - terminalHeight()
    var
        command = '\0'
        topLine = minTopLine
    while true:
        redraw(lineBuffer, topLine)
        case getch():
            of 'q':
                break
            of 'j':
                inc topLine
                if topLine > maxTopLine: topLine = maxTopLine
            of 'k':
                dec topLine
                if topLine < minTopLine: topLine = minTopLine
            of 'f':
                topLine += terminalHeight()
                if topLine > maxTopLine: topLine = maxTopLine
            of 'b':
                topLine -= terminalHeight()
                if topLine < minTopLine: topLine = minTopLine
            of 'g':
                topLine = minTopLine
            of 'G':
                topLine = maxTopLine
            else:
                discard

dispatch(main, version=("version", nimbaseVersion))
