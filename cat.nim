import cligen, os
import common

proc catFile(file:string) =
    if not existsFile(file):
        err "cat: " & file & ": No such file or directory"
        quit 1
    const bufSize = 1024
    var
        f = open(file, fmRead)
        buffer {.noinit.}: array[bufSize, char]
    while true:
        let readBytes = readBuffer(f, addr(buffer[0]), bufSize)
        if readBytes == 0:
            break
        discard writeBuffer(stdout, addr(buffer[0]), readBytes)
        if readBytes < bufSize:
            break

proc catStdin() =
    for line in lines(stdin):
        writeLine(stdout, line)

proc main(files:seq[string]) =
    if files.len == 0:
        catStdin()
    for file in files:
        if file == "-":
            catStdin()
        else:
            catFile(file)

dispatch(main, version=("version", nimbaseVersion))
