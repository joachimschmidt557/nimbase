import cligen, sequtils, stint
import common

proc factor(num:StUint[64]):seq[StUint[64]] =
    result = @[]
    var i = num
    while i > 1:
        var x = 2.stuint(64)
        while x <= i:
            if i mod x == 0:
                result.add(x)
                i = i div x
                break
            else: x += 1.stuint(64)

proc main(numbers:seq[string]) =
    for number in numbers:
        echo number, ": ",
             map(factor(number.parse(StUint[64])), proc(x:StUint[64]):string = $x & " ").foldl(a & b)

dispatch(main)
