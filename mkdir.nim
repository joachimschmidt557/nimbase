import cligen, os
import common

proc main(dirs:seq[string]) =
    for dir in dirs:
        createDir(dir)

dispatch(main)
