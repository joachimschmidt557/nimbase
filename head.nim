import cligen, os
import common

proc main(files:seq[string], lines=10, bytes=0, quiet=false) =
#    if files.len == 0:
#        files.add "-"
    for file in files:
        if files.len > 1 and not quiet:
            writeLine(stdout, "==> " & (if file == "-": "standard input" else: file) & " <==")
        var f = if file == "-": stdin
                else: open(file, fmRead)
        if bytes > 0:
            var currentByte = 0
            while not f.endOfFile and currentByte < bytes:
                write(stdout, f.readChar)
                inc currentByte
        elif lines > 0:
            var currentLine = 0
            for line in lines(f):
                if currentLine >= lines:
                    break
                writeLine(stdout, line)
                inc currentLine

dispatch(main, version=("version", nimbaseVersion))
