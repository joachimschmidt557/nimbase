import cligen, os, times
import common

proc main(files:seq[string]) =
    for file in files:
        if not existsFile(file):
            discard open(file, fmWrite)
        setLastModificationTime(file, getTime())

dispatch(main, version=("version", nimbaseVersion))
