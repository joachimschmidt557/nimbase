packageName   = "nimbase"
version       = "0.0.1"
author        = "joachimschmidt557"
description   = "Basic utilities written in Nim"
license       = "Unlicense"

requires      "nim >= 0.19.4"
requires      "cligen >= 0.9.19"
requires      "stint >= 0.0.1"

bin           = @["add", "cat", "cp", "div", "echo", "factor", "false", "head", "mkdir", "more", "mul", "nl", "pwd", "rev", "rm", "seq", "shuf", "sleep", "sub", "tail", "tac", "tee", "touch", "true", "wc", "yes"]

binDir        = "bin"
