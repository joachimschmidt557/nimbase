import cligen, sequtils
import common

proc main(strings:seq[string]) =
    if strings.len == 0:
        while true:
            echo "y"
    else:
        while true:
            echo strings.foldl(a & b)

dispatch(main, version=("version", nimbaseVersion))
