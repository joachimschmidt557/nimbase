# Common constants

const
    nimbaseVersion* = "0.0.1"

proc err*(x:string) =
    writeLine(stderr, x)
