import cligen, os
import common

proc catFile(file:string) =
    if not existsFile(file):
        err "tac: " & file & ": No such file or directory"
        quit 1
    var
        f = open(file, fmRead)
        reverseLines: seq[string]
    for line in lines(f):
        reverseLines.add(line)
    for i in low(reverseLines)..high(reverseLines):
        writeLine(stdout, reverseLines[high(reverseLines) - i])

proc catStdin() =
    var
        reverseLines: seq[string]
    for line in lines(stdin):
        reverseLines.add(line)
    for i in low(reverseLines)..high(reverseLines):
        writeLine(stdout, reverseLines[high(reverseLines) - i])

proc main(files:seq[string]) =
    if files.len == 0:
        catStdin()
    for file in files:
        if file == "-":
            catStdin()
        else:
            catFile(file)

dispatch(main, version=("version", nimbaseVersion))
