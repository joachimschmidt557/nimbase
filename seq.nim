import cligen
import common

proc main(numbers:seq[int], separator="\n") =
    if numbers.len < 1:
        err "seq: missing operand"
        quit 1
    if numbers.len > 3:
        err "seq: extra operand"
        quit 1
    let
        last = numbers[numbers.high]
        first = if numbers.len > 1: numbers[0]
                else: 1
        increment = if numbers.len == 3: numbers[1]
                    else: 1
    var i = first
    while i <= last:
        write(stdout, i)
        write(stdout, separator)
        i = i + increment

dispatch(main, version=("version", nimbaseVersion))
