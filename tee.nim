import cligen, os
import common

proc main(files:seq[string]) =
    var
        fileObjs:seq[File]
    for file in files:
        fileObjs.add(open(file, fmWrite))
    for line in lines(stdin):
        writeLine(stdout, line)
        for file in fileObjs:
            writeLine(file, line)

dispatch(main, version=("version", nimbaseVersion))
