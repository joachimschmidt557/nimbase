import cligen, sequtils
import common

proc main(strings:seq[string], newline=true, nospaces=false) =
    if newline:
        writeLine(stdout, strings.foldl(a & (if nospaces: "" else: " ") & b))
    else:
        write(stdout, strings.foldl(a & (if nospaces: "" else: " ") & b))
    flushFile(stdout)

dispatch(main, version=("version", nimbaseVersion))
