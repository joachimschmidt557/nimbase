import cligen
import common

proc main(files:seq[string]) =
    for file in files:
        for line in lines(file):
            echo line

dispatch(main, version=("version", nimbaseVersion))
